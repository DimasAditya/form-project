from django import forms
from django.forms.widgets import TextInput, DateTimeInput

from .models import ModJadwal

class JadwalPribadiForms(forms.ModelForm):

	class Meta:
		model = ModJadwal
		fields = ('event', 'tanggal', 'tempat', 'kategori')
		widgets = {
			'event': TextInput(attrs={'class': 'form-control', 'type' : 'text'}),
            'tanggal': DateTimeInput(attrs={'class': 'form-control', 'type' : 'date'}),
			'tempat': TextInput(attrs={'class': 'form-control', 'type' : 'text'}),
			'kategori': TextInput(attrs={'class': 'form-control', 'type' : 'text'})
		}


