from django.urls import path, re_path 
from .views import postJadwal, delete, lihatJadwal


urlpatterns = [
    path('delete/', delete, name='deleteSchedule'),
    path('show', lihatJadwal, name='lihatJadwal'),
    path('create', postJadwal, name='postJadwal'),
    path('', postJadwal, name = 'navbar'),
]
