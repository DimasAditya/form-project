from django.shortcuts import render, redirect, render_to_response
from django.views.generic.edit import CreateView
from .models import ModJadwal
from .forms import JadwalPribadiForms


# post.author = request.user
# post.published_date = timezone.now()

def index(request):
  return render(request, 'main.html')

def postJadwal(request):
	if request.method == "POST":
		form = JadwalPribadiForms(request.POST)
		if form.is_valid():
			post = form.save(commit=False)
			
			post.save()
			return redirect('lihatJadwal')
	else:
		form = JadwalPribadiForms()
		data = ModJadwal.objects.all()
		return render(request, "jadwalkegiatan.html", {'form': form, 'data': data})

def lihatJadwal(request):    
  tmp = ModJadwal.objects.all()
  return render(request, 'posted_jadwal.html', {'table': tmp})

def delete(request):
  ModJadwal.objects.all().delete()
  form = JadwalPribadiForms(request.POST)
  return redirect('lihatJadwal')

