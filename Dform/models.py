from django.db import models
from datetime import date

# Create your models here.
class ModJadwal(models.Model):
  event = models.CharField( max_length = 50)
  tanggal = models.DateTimeField()
  tempat = models.CharField( max_length = 50)
  kategori = models.CharField( max_length = 30)
  
  

  def __str__(self):
    return self.event